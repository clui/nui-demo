## Requirements

- nodeJS > v.8.9 (https://nodejs.org/en/download/)
- Amplify CLI (https://aws-amplify.github.io/docs/js/authentication)
- Recommended IDE: VSCode

## Installation

1. Install nodeJS
2. In terminal, in the project root, run `yarn`
3. Install Amplify CLI by `yarn add aws-amplify`
4. Run `amplify configure` (https://www.youtube.com/watch?v=fWbM5DLh25U)
5. During the process it will ask you some question: AWS region -> `ap-southeast-2`, user name -> create your own user, follow the video instructions above
6. Run `amplify init`, use an existing environment? -> `Y` -> `master`
7. Installation done

## Development workflow

1. Start the development server, run `yarn start`
2. Access the development server in http://localhost:3000/
3. Push change to `master` branch, this will trigger a build in Amplify
4. It will build to production server in https://master.d2u4gm9amppc6q.amplifyapp.com/

### End user page

http://localhost:3000/demo/
http://localhost:3000/demo/demo1.html

### Deployed library

https://master.d2u4gm9amppc6q.amplifyapp.com/js/bundle.js

## Useful script

yarn cache clean
yarn install

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.
