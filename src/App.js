import React from 'react';
import {withCookies} from 'react-cookie';
import { withAuthenticator } from 'aws-amplify-react';
import { Auth, API } from 'aws-amplify';
import { Button, Grid, Typography, Paper, AppBar, Toolbar, IconButton, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Drawer, List, Divider, ListItem, ListItemIcon, ListItemText } from 'clui-ui';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import MenuIcon from '@material-ui/icons/Menu';
import InboxIcon  from '@material-ui/icons/Inbox';
import MailIcon   from '@material-ui/icons/Mail';

const useStyles = makeStyles(theme => createStyles({
  root: {
    backgroundColor: theme.palette.background.default,
    color: theme.palette.text.primary,
    padding: theme.spacing(4)
  },
  paper: {
    padding: theme.spacing(2)
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
}));

function App(props) {
  const {cookies} = props;
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false,
  });
  const [items, setItems] = React.useState([]);
  const [credentials, setCredentials] = React.useState({});
  const [userInfo, setUserInfo] = React.useState({attributes:{}});

  const mainMenuItems = ['Inbox', 'Starred', 'Send email', 'Drafts'];
  const secondaryMenuItems = ['All mail', 'Trash', 'Spam', ];

  const signOut = async () => {
    await Auth.signOut();
  }

  const currentCredentials = async () => {
    const res = await Auth.currentCredentials();
    setCredentials(res);
    console.log('currentCredentials', res)
  }

  const currentUserInfo = async () => {
    const res = await Auth.currentUserInfo();
    console.log('currentUserInfo', res)
    setUserInfo(res);
  }

  const getItems = async () => {
    let apiName = 'nuidemoapi';
    let path = '/items';
    let myInit = { // OPTIONAL
        headers: {} // OPTIONAL
    }
    const res = await API.get(apiName, path, myInit);
    console.log('getItems', res)
    setItems(res.data)
  }

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const toggleDrawer = (side, open) => event => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    setState({ ...state, [side]: open });
  };

  const sideList = side => (
    <div
      className={classes.list}
      role="presentation"
      onClick={toggleDrawer(side, false)}
      onKeyDown={toggleDrawer(side, false)}
    >
      <List>
        {mainMenuItems.map((text, index) => (
          <ListItem button key={text}>
            <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
            <ListItemText primary={text} />
          </ListItem>
        ))}
      </List>
      <Divider />
      <List>
        {secondaryMenuItems.map((text, index) => (
          <ListItem button key={text}>
            <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
            <ListItemText primary={text} />
          </ListItem>
        ))}
      </List>
    </div>
  );

  React.useEffect(() => {
    currentCredentials();
    getItems();
    currentUserInfo();
  }, []);

  return (
    <React.Fragment>
      <header>
        <AppBar position="static">
            <Toolbar>
                <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu" onClick={toggleDrawer('left', true)}>
                <MenuIcon />
                </IconButton>
                <Typography variant="h6" className={classes.title}>
                This is a demo Nui embed app
                </Typography>
                <Button color="inherit" onClick={signOut} >Sign out</Button>
            </Toolbar>
        </AppBar>
      </header>
      <div className={classes.root}>
        <Box pb={4}>
          <Typography variant="body1" component="p">Global variable: <b>{window.globalVar || "-"}</b></Typography>
          <Typography variant="body1" component="p">Local storage variable: <b>{window.localStorage.getItem('localStorageVar') || "-"}</b></Typography>
          <Typography variant="body1" component="p">Cookie variable: <b>{cookies.get('cookieVar') || "-"}</b></Typography>
          <Typography variant="body1" component="p">IdentityId: <b>{credentials.identityId}</b></Typography>
          <Typography variant="body1" component="p">Username: <b>{userInfo.username}</b></Typography>
          <Typography variant="body1" component="p">Username: <b>{userInfo.attributes.email}</b></Typography>
  <Typography variant="body1" component="p"></Typography>
        </Box>
        <Grid container spacing={4}>
          {items.map((item, index) => (
            <Grid item xs={3} key={item.id}>
              <Paper className={classes.paper}>
                <Box pb={2}>
                  <Typography variant="h5" component="h3">{item.title}</Typography>
                </Box>
                <Box pb={2}>
                  <Typography variant="body1" component="p">os ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti.</Typography>
                </Box>
                <Button variant="contained" color="primary" size="medium" onClick={handleClickOpen}>
                  Open dialog
                </Button>
              </Paper>
            </Grid>
          ))}          
        </Grid>
      </div>
      <Dialog
          open={open}
          onClose={handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
      >
          <DialogTitle id="alert-dialog-title">{"This is a sample dialog(modal)"}</DialogTitle>
          <DialogContent>
          <DialogContentText id="alert-dialog-description">
              Let Google help apps determine location. This means sending anonymous location data to
              Google, even when no apps are running.
          </DialogContentText>
          </DialogContent>
          <DialogActions>
          <Button onClick={handleClose} variant="contained" color="primary" autoFocus>
              Agree
          </Button>
          <Button onClick={handleClose} variant="outlined" color="primary">
              Disagree
          </Button>
          </DialogActions>
      </Dialog>
      <Drawer open={state.left} onClose={toggleDrawer('left', false)}>
        {sideList('left')}
      </Drawer>
    </React.Fragment>
  );
}

export default withCookies(withAuthenticator(App));
